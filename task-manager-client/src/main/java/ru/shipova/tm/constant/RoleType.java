package ru.shipova.tm.constant;

import org.jetbrains.annotations.NotNull;

public enum RoleType {
    ADMIN("АДМИНИСТРАТОР"),
    USER("ОБЫЧНЫЙ ПОЛЬЗОВАТЕЛЬ");

    private @NotNull final String name;

    RoleType(@NotNull final String name) {
        this.name = name;
    }

    @NotNull
    public String displayName() {
        return name;
    }
}
