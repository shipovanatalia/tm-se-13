package ru.shipova.tm.constant;

import org.jetbrains.annotations.NotNull;

import java.io.File;

public enum DataConstant {
    FILE_BINARY("." + File.separator + "data.bin"),
    FILE_XML("." + File.separator + "data.xml"),
    FILE_JSON("."+ File.separator + "data.json");

    private @NotNull
    final String name;

    DataConstant(@NotNull final String name) {
        this.name = name;
    }

    @NotNull
    public String displayName() {
        return name;
    }
}
