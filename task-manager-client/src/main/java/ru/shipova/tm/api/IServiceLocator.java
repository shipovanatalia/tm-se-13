package ru.shipova.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.endpoint.*;
import ru.shipova.tm.service.SessionService;
import ru.shipova.tm.service.TerminalService;

public interface IServiceLocator {
    @NotNull TerminalService getTerminalService();
    @NotNull SessionService getSessionService();
    @Nullable UserEndpoint getUserEndpoint();
    @Nullable ProjectEndpoint getProjectEndpoint();
    @Nullable TaskEndpoint getTaskEndpoint();
    @Nullable SessionEndpoint getSessionEndpoint();
    @Nullable DomainEndpoint getDomainEndpoint();
    @Nullable AdminUserEndpoint getAdminUserEndpoint();
}
