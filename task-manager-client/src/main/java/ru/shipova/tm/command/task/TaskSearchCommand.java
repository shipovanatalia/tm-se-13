package ru.shipova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.endpoint.*;

import java.util.List;

public class TaskSearchCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-search";
    }

    @Override
    public String getDescription() {
        return "Search task by part of name.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            @Nullable final AdminUserEndpoint adminUserEndpoint = serviceLocator.getAdminUserEndpoint();
            if (adminUserEndpoint == null) return;
            @Nullable final Session session = serviceLocator.getSessionService().getSession();
            try {
                if (!adminUserEndpoint.haveAccessToUsualCommand(session, needAuthorize())) {
                    System.out.println("ACCESS DENIED. REQUIRED AUTHORIZATION");
                    return;
                }
                System.out.println("[TASK SEARCH]");
                @NotNull final String partOfData = serviceLocator.getTerminalService().nextLine();
                @Nullable final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
                if (taskEndpoint == null) return;
                @Nullable final List<Task> taskList;
                taskList = taskEndpoint.searchTask(session, partOfData);
                if (taskList == null || taskList.isEmpty()) {
                    System.out.println("TASKS ARE NOT FOUND");
                    return;
                }
                int index = 1;
                for (@NotNull final Task task : taskList) {
                    System.out.println(index++ + ". " + task.getName());
                }
                System.out.println();
            } catch (AccessForbiddenException_Exception e) {
                System.out.println("ACCESS DENIED.");
            }
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
