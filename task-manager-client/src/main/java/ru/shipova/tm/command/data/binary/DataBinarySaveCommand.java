package ru.shipova.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.constant.Serializer;
import ru.shipova.tm.endpoint.*;

public class DataBinarySaveCommand extends AbstractCommand {
    @Override
    @Nullable
    public String getName() {
        return "bin-save";
    }

    @Override
    @Nullable
    public String getDescription() {
        return "Save data to binary file.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator != null) {
            @Nullable final AdminUserEndpoint adminUserEndpoint = serviceLocator.getAdminUserEndpoint();
            @Nullable final DomainEndpoint domainEndpoint = serviceLocator.getDomainEndpoint();
            @Nullable final Session session = serviceLocator.getSessionService().getSession();
            if (adminUserEndpoint == null) return;
            if (!adminUserEndpoint.haveAccessToUsualCommand(session, needAuthorize())) {
                System.out.println("ACCESS DENIED. REQUIRED AUTHORIZATION");
                return;
            }
            if (!adminUserEndpoint.haveAccessToAdminCommand(session, isOnlyAdminCommand())) {
                System.out.println("ACCESS DENIED. NEED ADMINISTRATOR RIGHTS.");
                return;
            }
            System.out.println("[DATA BINARY SAVE]");
            if (domainEndpoint == null) return;
            @NotNull final Domain domain = new Domain();
            @NotNull final String serializer = Serializer.BINARY.displayName();
            try {
                domainEndpoint.serialize(session, domain, serializer);
            } catch (AccessForbiddenException_Exception e) {
                System.out.println("ACCESS DENIED.");
            }
            System.out.println("[OK]");
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }

    @Override
    public boolean isOnlyAdminCommand() {
        return true;
    }
}
