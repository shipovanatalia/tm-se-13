package ru.shipova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.endpoint.*;

import java.util.List;

public final class TaskListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            @Nullable final AdminUserEndpoint adminUserEndpoint = serviceLocator.getAdminUserEndpoint();
            if (adminUserEndpoint == null) return;
            @Nullable final Session session = serviceLocator.getSessionService().getSession();

            try {
                if (!adminUserEndpoint.haveAccessToUsualCommand(session, needAuthorize())) {
                    System.out.println("ACCESS DENIED. REQUIRED AUTHORIZATION");
                    return;
                }
                System.out.println("[TASK LIST]");
                @Nullable final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
                if (taskEndpoint == null) return;
                if (session == null) return;
                sortTasks(taskEndpoint, session);
            } catch (AccessForbiddenException_Exception e) {
                System.out.println("ACCESS DENIED.");
            }
        }
    }

    private void sortTasks(@NotNull final TaskEndpoint taskEndpoint, @NotNull final Session session) {
        if (serviceLocator != null) {
            System.out.println("PLEASE CHOOSE TYPE OF SORT OF TASKS: ");
            System.out.println("1. DATE OF CREATE;");
            System.out.println("2. DATE OF BEGIN;");
            System.out.println("3. DATE OF END;");
            System.out.println("4. STATUS.");
            System.out.println("ENTER NAME OF SORT OR NO");
            @NotNull final String typeOfSort = serviceLocator.getTerminalService().nextLine();
            try{
            @Nullable List<Task> sortedList = null;
            try {
                sortedList = taskEndpoint.getSortedTaskListOfUser(session, typeOfSort);
            } catch (CommandCorruptException_Exception e) {
                System.out.println("WRONG COMMAND.");
                System.out.println();
                sortTasks(taskEndpoint, session);
            }

            int index = 1;
            if (sortedList == null) return;
            if (sortedList.isEmpty()) System.out.println("TASK LIST IS EMPTY");

            for (@Nullable final Task task : sortedList) {
                if (task == null) return;
                System.out.println(index++ + ". " + task.getName());
            }
            System.out.println();
            } catch (AccessForbiddenException_Exception e) {
                System.out.println("ACCESS DENIED.");
            }
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
