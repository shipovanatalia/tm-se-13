package ru.shipova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.endpoint.*;

import java.util.List;

public final class ProjectListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            @Nullable final AdminUserEndpoint adminUserEndpoint = serviceLocator.getAdminUserEndpoint();
            if (adminUserEndpoint == null) return;
            @Nullable final Session session = serviceLocator.getSessionService().getSession();
            try {
                if (!adminUserEndpoint.haveAccessToUsualCommand(session, needAuthorize())) {
                    System.out.println("ACCESS DENIED. REQUIRED AUTHORIZATION");
                    return;
                }
                System.out.println("[PROJECT LIST]");
                @Nullable final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
                if (projectEndpoint == null) return;
                if (session == null) return;
                sortProjects(projectEndpoint, session);
            } catch (AccessForbiddenException_Exception e) {
                System.out.println("ACCESS DENIED.");
            }
        }
    }

    private void sortProjects(@NotNull final ProjectEndpoint projectEndpoint, @NotNull final Session session) {
        if (serviceLocator != null) {
            System.out.println("PLEASE CHOOSE TYPE OF SORT OF PROJECTS: ");
            System.out.println("1. DATE OF CREATE;");
            System.out.println("2. DATE OF BEGIN;");
            System.out.println("3. DATE OF END;");
            System.out.println("4. STATUS.");
            System.out.println("ENTER NAME OF SORT OR NO");
            @NotNull final String typeOfSort = serviceLocator.getTerminalService().nextLine();
            try {
                @Nullable List<Project> sortedList = null;
                try {
                    sortedList = projectEndpoint.getSortedProjectListOfUser(session, typeOfSort);
                } catch (CommandCorruptException_Exception e) {
                    System.out.println("WRONG COMMAND.");
                    System.out.println();
                    sortProjects(projectEndpoint, session);
                }

                int index = 1;
                if (sortedList == null) return;
                if (sortedList.isEmpty()) System.out.println("PROJECT LIST IS EMPTY");

                for (@Nullable final Project project : sortedList) {
                    if (project == null) return;
                    System.out.println(index++ + ". " + project.getName());
                }
                System.out.println();
            } catch (AccessForbiddenException_Exception e) {
                System.out.println("ACCESS DENIED.");
            }
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
