package ru.shipova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.endpoint.*;

import java.util.List;

public final class TaskShowCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "task-show";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all tasks of project.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            @Nullable final AdminUserEndpoint adminUserEndpoint = serviceLocator.getAdminUserEndpoint();
            if (adminUserEndpoint == null) return;
            @Nullable final Session session = serviceLocator.getSessionService().getSession();
            try {
                if (!adminUserEndpoint.haveAccessToUsualCommand(session, needAuthorize())) {
                    System.out.println("ACCESS DENIED. REQUIRED AUTHORIZATION");
                    return;
                }
                System.out.println("ENTER NAME OF PROJECT:");
                @NotNull final String projectName = serviceLocator.getTerminalService().nextLine();
                @Nullable final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
                if (taskEndpoint == null) return;
                @NotNull final List<Task> taskList = taskEndpoint.showAllTasksOfProject(session, projectName);
                if (taskList.isEmpty()) {
                    System.out.println("PROJECT " + projectName + " DOES NOT CONTAIN TASKS");
                    return;
                }
                System.out.println("PROJECT " + projectName + " CONTAINS TASKS:");
                for (@Nullable final Task task : taskList) {
                    if (task == null) return;
                    System.out.println(task.getName());
                }
            } catch (AccessForbiddenException_Exception e) {
                System.out.println("ACCESS DENIED.");
            } catch (ProjectDoesNotExistException_Exception e) {
                System.out.println("PROJECT DOES NOT EXISTS.");
            }
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
