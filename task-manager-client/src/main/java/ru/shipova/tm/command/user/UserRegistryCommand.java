package ru.shipova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.endpoint.*;

public final class UserRegistryCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-registry";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Registry new user.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            @Nullable final AdminUserEndpoint adminUserEndpoint = serviceLocator.getAdminUserEndpoint();
            try {
                @Nullable final Session session = serviceLocator.getSessionService().getSession();
                if (adminUserEndpoint == null) return;
                if (!adminUserEndpoint.haveAccessToUsualCommand(session, needAuthorize())) {
                    System.out.println("ACCESS DENIED. REQUIRED AUTHORIZATION");
                    return;
                }
                if (!adminUserEndpoint.haveAccessToAdminCommand(session, isOnlyAdminCommand())) {
                    System.out.println("ACCESS DENIED. NEED ADMINISTRATOR RIGHTS.");
                    return;
                }
                System.out.println("[REGISTRY USER]");
                System.out.println("ENTER LOGIN");
                @NotNull final String login = serviceLocator.getTerminalService().nextLine();
                System.out.println("ENTER PASSWORD");
                @NotNull final String password = serviceLocator.getTerminalService().nextLine();
                System.out.println("ENTER ROLE OF USER");
                @NotNull final String roleType = serviceLocator.getTerminalService().nextLine();
                @Nullable final UserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
                if (userEndpoint == null) return;
                userEndpoint.registryUser(session, login, password, roleType);
                System.out.println("[OK]");
            } catch (AccessForbiddenException_Exception e) {
                System.out.println("ACCESS DENIED.");
            } catch (LoginAlreadyExistsException_Exception e) {
                System.out.println("LOGIN ALREADY EXISTS.");
            }
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }

    @Override
    public boolean isOnlyAdminCommand() {
        return true;
    }
}
