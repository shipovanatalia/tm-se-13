package ru.shipova.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.exception.LoginAlreadyExistsException;
import ru.shipova.tm.repository.UserRepository;
import ru.shipova.tm.util.DataBaseUtil;
import ru.shipova.tm.util.HashUtil;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
public final class UserService implements IUserService {

    private @Nullable UserRepository userRepository;
    private @Nullable SqlSessionFactory sqlSessionFactory;
    private @Nullable SqlSession sqlSession;

    @Override
    public void createUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final RoleType roleType
    ) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (roleType == null) return;
        @NotNull final String userId = UUID.randomUUID().toString();
        @Nullable final String passwordHash = HashUtil.md5(password);
        if (passwordHash == null) return;
        @NotNull final User user = new User(userId, login, passwordHash, roleType);
        try {
            sqlSessionFactory = DataBaseUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            userRepository = sqlSession.getMapper(UserRepository.class);
            userRepository.persist(user);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public User authorize(@NotNull final String login, @NotNull final String password) {
        sqlSessionFactory = DataBaseUtil.getSqlSessionFactory();
        sqlSession = sqlSessionFactory.openSession();
        userRepository = sqlSession.getMapper(UserRepository.class);
        if (userRepository == null) return null;
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        @Nullable final User user = userRepository.findByLogin(login);
        sqlSession.close();
        return user;
    }

    @Override
    public boolean checkDataAccess(@NotNull final String login, @NotNull final String password) {
        if (login.isEmpty()) return false;
        if (password.isEmpty()) return false;
        @Nullable final User user = findByLogin(login);
        if (user == null) return false;
        @Nullable final String passwordHash = HashUtil.md5(password);
        if (passwordHash == null) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        sqlSessionFactory = DataBaseUtil.getSqlSessionFactory();
        sqlSession = sqlSessionFactory.openSession();
        userRepository = sqlSession.getMapper(UserRepository.class);
        if (userRepository == null) return null;
        if (login == null || login.isEmpty()) return null;
        @Nullable final User user = userRepository.findByLogin(login);
        sqlSession.close();
        return user;
    }

    @Nullable
    @Override
    public User findById(@NotNull final String userId) {
        sqlSessionFactory = DataBaseUtil.getSqlSessionFactory();
        sqlSession = sqlSessionFactory.openSession();
        userRepository = sqlSession.getMapper(UserRepository.class);
        if (userRepository == null) return null;
        @Nullable final User user = userRepository.findById(userId);
        sqlSession.close();
        return user;
    }

    @Nullable
    @Override
    public RoleType getRoleType(@NotNull final Session session) {
        @Nullable final User user = findById(session.getUserId());
        if (user == null) return null;
        @Nullable final RoleType roleType = user.getRoleType();
        return roleType;
    }

    @Nullable
    @Override
    public RoleType getRoleType(@Nullable final String role) {
        if (role == null || role.isEmpty()) return null;
        RoleType roleType = null;
        switch (role.toUpperCase()) {
            case "USER":
                roleType = RoleType.USER;
                break;
            case "ADMIN":
                roleType = RoleType.ADMIN;
                break;
        }
        return roleType;
    }


    @Override
    public void registryUser(@Nullable final String login, @Nullable final String password, @Nullable final String role) throws LoginAlreadyExistsException {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (role == null || role.isEmpty()) return;
        sqlSessionFactory = DataBaseUtil.getSqlSessionFactory();
        sqlSession = sqlSessionFactory.openSession();
        userRepository = sqlSession.getMapper(UserRepository.class);
        if (userRepository.findByLogin(login) != null) throw new LoginAlreadyExistsException();
        if (userRepository == null) return;
        @Nullable final String passwordHash = HashUtil.md5(password);
        @NotNull final String userId = UUID.randomUUID().toString();
        @Nullable final RoleType roleType = getRoleType(role);
        try {
            userRepository.persist(new User(userId, login, passwordHash, roleType));
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void updateUser(@Nullable final String login, @Nullable final String role) {
        if (role == null || role.isEmpty()) return;
        if (login == null || login.isEmpty()) return;
        sqlSessionFactory = DataBaseUtil.getSqlSessionFactory();
        sqlSession = sqlSessionFactory.openSession();
        userRepository = sqlSession.getMapper(UserRepository.class);
        if (userRepository == null) return;
        @Nullable final RoleType roleType = getRoleType(role);
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) return;
        user.setRoleType(roleType);
        try {
            userRepository.update(user);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void setNewPassword(@Nullable final String login, @NotNull final String password) {
        if (password.isEmpty()) return;
        if (login == null || login.isEmpty()) return;
        sqlSessionFactory = DataBaseUtil.getSqlSessionFactory();
        sqlSession = sqlSessionFactory.openSession();
        userRepository = sqlSession.getMapper(UserRepository.class);
        if (userRepository == null) return;
        @Nullable final String passwordHash = HashUtil.md5(password);
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) return;
        user.setPasswordHash(passwordHash);
        try {
            userRepository.update(user);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @Nullable
    public List<User> getUserList() {
        sqlSessionFactory = DataBaseUtil.getSqlSessionFactory();
        sqlSession = sqlSessionFactory.openSession();
        userRepository = sqlSession.getMapper(UserRepository.class);
        if (userRepository == null) return null;
        @Nullable final List<User> userList = userRepository.findAll();
        sqlSession.close();
        return userList;
    }

    @Override
    public void load(@Nullable final List<User> userList) {
        if (userList == null) return;
        sqlSessionFactory = DataBaseUtil.getSqlSessionFactory();
        sqlSession = sqlSessionFactory.openSession();
        userRepository = sqlSession.getMapper(UserRepository.class);
        if (userRepository == null) return;
        try {
            for (@Nullable final User user : userList) {
                if (user == null) return;
                userRepository.update(user);
            }
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }
}