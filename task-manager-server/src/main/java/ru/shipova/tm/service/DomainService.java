package ru.shipova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.ISerializer;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.api.service.IDomainService;
import ru.shipova.tm.constant.Serializer;
import ru.shipova.tm.entity.*;
import ru.shipova.tm.serializer.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DomainService implements IDomainService {

    @NotNull
    private final IServiceLocator serviceLocator;

    public DomainService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void deserialize(@NotNull final Session session, @NotNull final String serializer) {
        if (Serializer.BINARY.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer binarySerializer = new BinarySerializer();
            @Nullable Domain domain;
            try {
                domain = binarySerializer.deserialize();
                load(domain);
            } catch (IOException e) {
                System.out.println("OOPS, YOU HAVE A PROBLEM WITH DESERIALIZATION.");
            }
        }
        if (Serializer.JSON_FASTER_XML.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer jsonFasterXmlSerializer = new JsonFasterXmlSerializer();
            @Nullable Domain domain;
            try {
                domain = jsonFasterXmlSerializer.deserialize();
                load(domain);
            } catch (IOException e) {
                System.out.println("OOPS, YOU HAVE A PROBLEM WITH DESERIALIZATION.");
            }
        }
        if (Serializer.JSON_JAX_B.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer jsonJaxBSerializer = new JsonJaxBSerializer();
            @Nullable Domain domain;
            try {
                domain = jsonJaxBSerializer.deserialize();
                load(domain);
            } catch (IOException e) {
                System.out.println("OOPS, YOU HAVE A PROBLEM WITH DESERIALIZATION.");
            }
        }
        if (Serializer.XML_FASTER_XML.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer xmlFasterXmlSerializer = new XmlFasterXmlSerializer();
            @Nullable Domain domain;
            try {
                domain = xmlFasterXmlSerializer.deserialize();
                load(domain);
            } catch (IOException e) {
                System.out.println("OOPS, YOU HAVE A PROBLEM WITH DESERIALIZATION.");
            }
        }
        if (Serializer.XML_JAX_B.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer xmlJaxBSerializer = new XmlJaxBSerializer();
            @Nullable Domain domain;
            try {
                domain = xmlJaxBSerializer.deserialize();
                load(domain);
            } catch (IOException e) {
                System.out.println("OOPS, YOU HAVE A PROBLEM WITH DESERIALIZATION.");
            }
        }
    }

    @Override
    public void load(@Nullable final Domain domain) {
        if (domain == null) return;
        serviceLocator.getIUserService().load(domain.getUserList());
        serviceLocator.getIProjectService().load(domain.getProjectList());
        serviceLocator.getITaskService().load(domain.getTaskList());
    }

    @Override
    public void serialize(@NotNull final Session session, @NotNull final Domain domain, @NotNull String serializer) throws IOException {
        domain.setUserId(session.getUserId());
        export(domain);
        if (Serializer.BINARY.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer binarySerializer = new BinarySerializer();
            binarySerializer.serialize(domain);
        }
        if (Serializer.JSON_FASTER_XML.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer jsonFasterXmlSerializer = new JsonFasterXmlSerializer();
            jsonFasterXmlSerializer.serialize(domain);
        }
        if (Serializer.JSON_JAX_B.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer jsonJaxBSerializer = new JsonJaxBSerializer();
            jsonJaxBSerializer.serialize(domain);
        }
        if (Serializer.XML_FASTER_XML.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer xmlFasterXmlSerializer = new XmlFasterXmlSerializer();
            xmlFasterXmlSerializer.serialize(domain);
        }
        if (Serializer.XML_JAX_B.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer xmlJaxBSerializer = new XmlJaxBSerializer();
            xmlJaxBSerializer.serialize(domain);
        }
    }

    @Override
    public void export(@Nullable final Domain domain) {
        if (domain == null) return;
        @Nullable final List<User> userList = serviceLocator.getIUserService().getUserList();
        @Nullable final List<Project> projectList = new ArrayList<>();
        @Nullable final List<Task> taskList = new ArrayList<>();
        if (userList == null) return;
        for (@Nullable final User user : userList) {
            if (user == null) return;
            @Nullable final List<Project> projectListOfUser =
                    serviceLocator.getIProjectService().getProjectListOfUser(user.getId());
            @Nullable final List<Task> taskListOfUser
                    = serviceLocator.getITaskService().getTaskListOfUser(user.getId());
            if (projectListOfUser == null) return;
            if (taskListOfUser == null) return;
            projectList.addAll(projectListOfUser);
            taskList.addAll(taskListOfUser);
        }
        domain.setUserList(userList);
        domain.setProjectList(projectList);
        domain.setTaskList(taskList);
    }
}
