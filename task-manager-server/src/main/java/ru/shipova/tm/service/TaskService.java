package ru.shipova.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import ru.shipova.tm.api.service.ITaskService;
import ru.shipova.tm.constant.TypeOfSort;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.exception.CommandCorruptException;
import ru.shipova.tm.exception.ProjectDoesNotExistException;
import ru.shipova.tm.repository.ProjectRepository;
import ru.shipova.tm.repository.TaskRepository;
import ru.shipova.tm.util.DataBaseUtil;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public final class TaskService implements ITaskService {
    private @Nullable
    TaskRepository taskRepository;
    private @Nullable
    ProjectRepository projectRepository;
    private @Nullable
    SqlSessionFactory sqlSessionFactory;
    private @Nullable
    SqlSession sqlSession;

    @NotNull
    @Override
    public List<Task> showAllTasksOfProject(@NotNull final String projectName) throws ProjectDoesNotExistException {
        sqlSessionFactory = DataBaseUtil.getSqlSessionFactory();
        sqlSession = sqlSessionFactory.openSession();
        taskRepository = sqlSession.getMapper(TaskRepository.class);
        projectRepository = sqlSession.getMapper(ProjectRepository.class);
        if (projectRepository == null) return new ArrayList<>();
        if (taskRepository == null) return new ArrayList<>();
        if (projectName.isEmpty()) throw new ProjectDoesNotExistException();
        @Nullable final Project project = projectRepository.getProjectByName(projectName);
        if (project == null) throw new ProjectDoesNotExistException();
        @NotNull final String projectId = project.getId();
        @NotNull List<Task> taskList = taskRepository.showAllTasksOfProject(projectId);
        sqlSession.close();
        return taskList;
    }

    @Nullable
    @Override
    public List<Task> getTaskListOfUser(@Nullable final String userId) {
        sqlSessionFactory = DataBaseUtil.getSqlSessionFactory();
        sqlSession = sqlSessionFactory.openSession();
        taskRepository = sqlSession.getMapper(TaskRepository.class);
        if (taskRepository == null) return new ArrayList<>();
        if (userId == null || userId.isEmpty()) return null;
        @NotNull List<Task> taskList = taskRepository.findAllByUserId(userId);
        sqlSession.close();
        return taskList;
    }

    @Override
    @Nullable
    public List<Task> getSortedTaskListOfUser(@NotNull final String userId, @NotNull final String typeOfSort) throws CommandCorruptException {
        sqlSessionFactory = DataBaseUtil.getSqlSessionFactory();
        sqlSession = sqlSessionFactory.openSession();
        taskRepository = sqlSession.getMapper(TaskRepository.class);
        if (taskRepository == null) return null;
        @Nullable final List<Task> taskList = getTaskListOfUser(userId);
        if (taskList == null) return null;
        @Nullable final TypeOfSort sort = TypeOfSort.getTypeOfSort(typeOfSort);
        if (sort == null) return taskList;
        switch (sort){
            case STATUS:
                return taskRepository.selectTaskListByStatus(userId);
            case DATE_OF_END:
                return taskRepository.selectTaskListByDateOfEnd(userId);
            case DATE_OF_BEGIN:
                return taskRepository.selectTaskListByDateOfBegin(userId);
            case DATE_OF_CREATE:
                return taskRepository.selectTaskListByDateOfCreate(userId);
        }
        sqlSession.close();
        return taskList;
    }

    @Override
    public void load(@Nullable final List<Task> taskList) {
        sqlSessionFactory = DataBaseUtil.getSqlSessionFactory();
        sqlSession = sqlSessionFactory.openSession();
        taskRepository = sqlSession.getMapper(TaskRepository.class);
        if (taskRepository == null) return;
        if (taskList == null) return;
        try {
            for (@Nullable final Task task : taskList) {
                if (task == null) return;
                taskRepository.persist(task);
            }
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public List<Task> search(@Nullable final String userId, @Nullable final String partOfData) {
        sqlSessionFactory = DataBaseUtil.getSqlSessionFactory();
        sqlSession = sqlSessionFactory.openSession();
        taskRepository = sqlSession.getMapper(TaskRepository.class);
        if (taskRepository == null) return null;
        if (userId == null || userId.isEmpty()) return null;
        if (partOfData == null || partOfData.isEmpty()) return null;
        @NotNull List<Task> taskList = taskRepository.search(userId, partOfData);
        sqlSession.close();
        return taskList;
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String taskName, @Nullable final String projectName) throws ProjectDoesNotExistException {
        sqlSessionFactory = DataBaseUtil.getSqlSessionFactory();
        sqlSession = sqlSessionFactory.openSession();
        taskRepository = sqlSession.getMapper(TaskRepository.class);
        projectRepository = sqlSession.getMapper(ProjectRepository.class);
        if (taskRepository == null) return;
        if (projectRepository == null) return;
        if (userId == null || userId.isEmpty()) return;
        if (taskName == null || taskName.isEmpty()) return;
        if (projectName == null || projectName.isEmpty()) return;
        @Nullable final Project project = projectRepository.getProjectByName(projectName);
        if (project == null) throw new ProjectDoesNotExistException();
        @NotNull final String projectId = project.getId();
        try {
            @NotNull final String taskId = UUID.randomUUID().toString();
            taskRepository.persist(new Task(taskId, taskName, projectId, userId));
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        sqlSessionFactory = DataBaseUtil.getSqlSessionFactory();
        sqlSession = sqlSessionFactory.openSession();
        taskRepository = sqlSession.getMapper(TaskRepository.class);
        if (taskRepository == null) return;
        if (sqlSession == null) return;
        if (userId == null || userId.isEmpty()) return;
        try {
            taskRepository.removeAllByUserId(userId);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String taskName) {
        sqlSessionFactory = DataBaseUtil.getSqlSessionFactory();
        sqlSession = sqlSessionFactory.openSession();
        taskRepository = sqlSession.getMapper(TaskRepository.class);
        if (taskRepository == null) return;
        if (taskName == null || taskName.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;

        @Nullable final Task task = taskRepository.getTaskByName(taskName);
        if (task == null) return;
        @NotNull final String taskId = task.getId();
        try {
            task.setDateOfEnd(new Date());
            if (!userId.equals(task.getUserId())) return;
            taskRepository.removeByTaskId(taskId);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }
}
