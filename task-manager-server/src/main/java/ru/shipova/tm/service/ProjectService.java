package ru.shipova.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import ru.shipova.tm.api.service.IProjectService;
import ru.shipova.tm.constant.TypeOfSort;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.exception.CommandCorruptException;
import ru.shipova.tm.repository.ProjectRepository;
import ru.shipova.tm.repository.TaskRepository;
import ru.shipova.tm.util.DataBaseUtil;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public final class ProjectService implements IProjectService {
    private @Nullable
    TaskRepository taskRepository;
    private @Nullable
    ProjectRepository projectRepository;
    private @Nullable
    SqlSessionFactory sqlSessionFactory;
    private @Nullable
    SqlSession sqlSession;

    @Override
    @Nullable
    public List<Project> getProjectListOfUser(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) return null;
        sqlSessionFactory = DataBaseUtil.getSqlSessionFactory();
        sqlSession = sqlSessionFactory.openSession();
        projectRepository = sqlSession.getMapper(ProjectRepository.class);
        if (projectRepository == null) return null;
        @Nullable final List<Project> projectList = projectRepository.getProjectListByUserId(userId);
        sqlSession.close();
        return projectList;
    }

    @Override
    @Nullable
    public List<Project> getSortedProjectListOfUser(@NotNull final String userId, @NotNull final String typeOfSort) throws CommandCorruptException {
        sqlSessionFactory = DataBaseUtil.getSqlSessionFactory();
        sqlSession = sqlSessionFactory.openSession();
        projectRepository = sqlSession.getMapper(ProjectRepository.class);
        if (projectRepository == null) return null;
        @Nullable final List<Project> projectList = getProjectListOfUser(userId);
        if (projectList == null) return null;
        @Nullable final TypeOfSort sort = TypeOfSort.getTypeOfSort(typeOfSort);
        if (sort == null) return projectList;
        switch (sort){
            case STATUS:
                return projectRepository.selectProjectListByStatus(userId);
            case DATE_OF_END:
                return projectRepository.selectProjectListByDateOfEnd(userId);
            case DATE_OF_BEGIN:
                return projectRepository.selectProjectListByDateOfBegin(userId);
            case DATE_OF_CREATE:
                return projectRepository.selectProjectListByDateOfCreate(userId);
        }
        sqlSession.close();
        return projectList;
    }

    @Override
    public void load(@Nullable final List<Project> projectList) {
        if (projectList == null) return;
        sqlSessionFactory = DataBaseUtil.getSqlSessionFactory();
        sqlSession = sqlSessionFactory.openSession();
        projectRepository = sqlSession.getMapper(ProjectRepository.class);
        try {
            for (@Nullable final Project project : projectList) {
                if (project == null) return;
                projectRepository.update(project);
            }
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public List<Project> search(@Nullable final String userId, @Nullable final String partOfData) {
        if (userId == null || userId.isEmpty()) return null;
        if (partOfData == null || partOfData.isEmpty()) return null;
        sqlSessionFactory = DataBaseUtil.getSqlSessionFactory();
        sqlSession = sqlSessionFactory.openSession();
        projectRepository = sqlSession.getMapper(ProjectRepository.class);
        if (projectRepository == null) return new ArrayList<>();
        @Nullable final List<Project> projectList = projectRepository.search(userId, partOfData);
        sqlSession.close();
        return projectList;
    }

    @Override
    public void create(@NotNull final String userId, @NotNull final String projectName) {
        @NotNull final String projectId = UUID.randomUUID().toString();
        sqlSessionFactory = DataBaseUtil.getSqlSessionFactory();
        sqlSession = sqlSessionFactory.openSession();
        projectRepository = sqlSession.getMapper(ProjectRepository.class);
        if (projectRepository == null) return;
        try {
            projectRepository.persist(new Project(projectId, userId, projectName));
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        sqlSessionFactory = DataBaseUtil.getSqlSessionFactory();
        sqlSession = sqlSessionFactory.openSession();
        taskRepository = sqlSession.getMapper(TaskRepository.class);
        projectRepository = sqlSession.getMapper(ProjectRepository.class);
        if (projectRepository == null) return;
        if (taskRepository == null) return;
        @Nullable final List<Project> projectList = projectRepository.getProjectListByUserId(userId);
        if (projectList == null) return;
        try {
            for (@Nullable final Project project : projectList) {
                if (project == null) return;
                taskRepository.removeAllTasksOfProject(project.getId());
            }
            projectRepository.removeAllByUserId(userId);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String projectName) {
        if (projectName == null || projectName.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        sqlSessionFactory = DataBaseUtil.getSqlSessionFactory();
        sqlSession = sqlSessionFactory.openSession();
        taskRepository = sqlSession.getMapper(TaskRepository.class);
        projectRepository = sqlSession.getMapper(ProjectRepository.class);
        if (projectRepository == null) return;
        if (taskRepository == null) return;
        @Nullable final List<Project> projectList = projectRepository.getProjectListByUserId(userId);
        if (projectList == null) return;
        try {
            for (@Nullable final Project project : projectList) {
                if (project == null) return;
                @NotNull final String projectId = project.getId();
                if (project.getName().equals(projectName))
                    projectRepository.removeByProjectId(projectId);
            }
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }
}
