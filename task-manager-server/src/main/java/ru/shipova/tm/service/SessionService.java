package ru.shipova.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.api.service.ISessionService;
import ru.shipova.tm.dto.Result;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.exception.AccessForbiddenException;
import ru.shipova.tm.repository.SessionRepository;
import ru.shipova.tm.util.DataBaseUtil;
import ru.shipova.tm.util.SignatureUtil;

import java.util.Date;
import java.util.UUID;

public class SessionService implements ISessionService {

    private @Nullable SessionRepository sessionRepository;
    private @Nullable IServiceLocator serviceLocator;
    private @Nullable SqlSessionFactory sqlSessionFactory;
    private @Nullable SqlSession sqlSession;

    @NotNull
    public SessionService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @Nullable
    public Session open(@Nullable final String login, @Nullable final String password) {
        sqlSessionFactory = DataBaseUtil.getSqlSessionFactory();
        sqlSession = sqlSessionFactory.openSession();
        sessionRepository = sqlSession.getMapper(SessionRepository.class);
        if (sessionRepository == null) return null;
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        try {
            if (serviceLocator == null) return null;
            @Nullable final User user = serviceLocator.getIUserService().authorize(login, password);
            if (user == null) return null;
            @NotNull final Session session = new Session();
            session.setId(UUID.randomUUID().toString());
            session.setUserId(user.getId());
            session.setTimestamp(new Date().getTime());
            @NotNull final String salt = "SomeVerySpicySalt";
            @Nullable final String signature = SignatureUtil.sign(session, salt, 100);
            if (signature == null || signature.isEmpty()) return null;
            session.setSignature(signature);
            sessionRepository.persist(session);
            sqlSession.commit();
            return session;
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return null;
    }

    @Override
    @Nullable
    public Result close(@NotNull final Session session) {
        sqlSessionFactory = DataBaseUtil.getSqlSessionFactory();
        sqlSession = sqlSessionFactory.openSession();
        sessionRepository = sqlSession.getMapper(SessionRepository.class);
        if (sessionRepository == null) return null;
        try {
            sessionRepository.removeBySessionId(session.getId());
            sqlSession.commit();
            return new Result();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return null;
    }

    @Override
    public void validate(@Nullable final Session session) throws AccessForbiddenException {
        sqlSessionFactory = DataBaseUtil.getSqlSessionFactory();
        sqlSession = sqlSessionFactory.openSession();
        sessionRepository = sqlSession.getMapper(SessionRepository.class);
        if (sessionRepository == null) return;
        if (session == null) throw new AccessForbiddenException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessForbiddenException();
        if (session.getTimestamp() == null) throw new AccessForbiddenException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessForbiddenException();
        @NotNull final String signatureSource = session.getSignature();
        SignatureUtil.sign(temp, "SomeVerySpicySalt", 100);
        @Nullable final String signatureTarget = temp.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessForbiddenException();
        if (sessionRepository.findBySessionId(session.getId()) == null) throw new AccessForbiddenException();
        sqlSession.close();
    }
}
