package ru.shipova.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.constant.Status;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Task extends AbstractFieldOfUser implements Serializable {

    public static final long serialVersionUID = 1;

    @Nullable
    private String projectId;

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    private String userId;

    public Task(@NotNull final String id,
                @Nullable final String name,
                @Nullable final String projectId,
                @Nullable final String userId) {
        this.id = id;
        this.name = name;
        this.projectId = projectId;
        this.userId = userId;
        this.dateOfCreate = new Date();
    }
}
