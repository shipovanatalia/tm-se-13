package ru.shipova.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.constant.Status;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class AbstractFieldOfUser extends AbstractEntity implements Serializable {
    @Nullable Status status;
    @Nullable Date dateOfCreate;
    @Nullable Date dateOfBegin;
    @Nullable Date dateOfEnd;
}
