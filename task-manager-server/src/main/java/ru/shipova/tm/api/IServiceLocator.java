package ru.shipova.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.shipova.tm.api.service.*;

public interface IServiceLocator {
    @NotNull IProjectService getIProjectService();
    @NotNull ITaskService getITaskService();
    @NotNull IUserService getIUserService();
    @NotNull IDomainService getIDomainService();
    @NotNull ISessionService getISessionService();
    @NotNull IAdminUserService getIAdminUserService();
}
