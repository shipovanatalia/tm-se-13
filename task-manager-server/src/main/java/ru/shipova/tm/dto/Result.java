package ru.shipova.tm.dto;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class Result {
    public Boolean success = true;
    public String message = "";
}
