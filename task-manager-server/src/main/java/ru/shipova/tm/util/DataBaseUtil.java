package ru.shipova.tm.util;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.repository.ProjectRepository;
import ru.shipova.tm.repository.SessionRepository;
import ru.shipova.tm.repository.TaskRepository;
import ru.shipova.tm.repository.UserRepository;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public final class DataBaseUtil {

    public static SqlSessionFactory getSqlSessionFactory(){
        final InputStream inputStream = DataBaseUtil.class.getClassLoader().getResourceAsStream("db.properties");
        final Properties property = new Properties();
        try {
            property.load(inputStream);
        } catch (IOException e) {
            System.out.println("ERROR WHILE LOADING DATA BASE PROPERTIES FILE");
            e.printStackTrace();
        }

        @Nullable final String user = property.getProperty("db.login");
        @Nullable final String password = property.getProperty("db.password");
        @Nullable final String url = property.getProperty("db.host");
        @Nullable final String driver = property.getProperty("jdbc.driver");
        final DataSource dataSource =
                new PooledDataSource(driver, url, user, password);
        final TransactionFactory transactionFactory =
                new JdbcTransactionFactory();
        final Environment environment =
                new Environment("development", transactionFactory, dataSource);
        final Configuration configuration = new Configuration(environment);
        configuration.addMapper(UserRepository.class);
        configuration.addMapper(ProjectRepository.class);
        configuration.addMapper(SessionRepository.class);
        configuration.addMapper(TaskRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }
}

