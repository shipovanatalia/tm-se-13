package ru.shipova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.exception.AccessForbiddenException;
import ru.shipova.tm.exception.LoginAlreadyExistsException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService(name = "UserEndpoint", targetNamespace = "http://endpoint.tm.shipova.ru/")
public class UserEndpoint extends AbstractEndpoint {
    public UserEndpoint() {
        super(null);
    }

    public UserEndpoint(@Nullable final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    public void registryUser(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "role", partName = "role") @Nullable final String role
    ) throws AccessForbiddenException, LoginAlreadyExistsException {
        if (serviceLocator == null) return;
        serviceLocator.getISessionService().validate(session);
        serviceLocator.getIUserService().registryUser(login, password, role);
    }

    @WebMethod
    public void updateUser(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "role", partName = "role") @Nullable final String role
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return;
        serviceLocator.getISessionService().validate(session);
        serviceLocator.getIUserService().updateUser(login, role);
    }

    @WebMethod
    @Nullable
    public String getRoleType(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return null;
        serviceLocator.getISessionService().validate(session);
        @NotNull final String roleType = serviceLocator.getIUserService().getRoleType(session).displayName();
        return roleType;
    }

    @WebMethod
    @Nullable
    public String getUserLogin(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return null;
        serviceLocator.getISessionService().validate(session);
        @Nullable final User user = serviceLocator.getIUserService().findById(session.getUserId());
        if (user == null) return null;
        @Nullable final String login = user.getLogin();
        return login;
    }

    @WebMethod
    public void setNewPassword(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String userLogin,
            @WebParam(name = "password", partName = "password") @NotNull final String password
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return;
        serviceLocator.getISessionService().validate(session);
        serviceLocator.getIUserService().setNewPassword(userLogin, password);
    }

    @WebMethod
    @Nullable
    public Session authorize(
            @WebParam(name = "login", partName = "login") @NotNull final String login,
            @WebParam(name = "password", partName = "password") @NotNull final String password
    ) {
        if (serviceLocator == null) return null;
        @Nullable User user = serviceLocator.getIUserService().authorize(login, password);
        if (user == null) return null;
        @Nullable final Session session = serviceLocator.getISessionService().open(login, password);
        if (session == null) return null;
        return session;
    }

    @WebMethod
    public void loadUserList(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "userList", partName = "userList") @NotNull final List<User> userList
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return;
        serviceLocator.getISessionService().validate(session);
        serviceLocator.getIUserService().load(userList);
    }

    @WebMethod
    @Nullable
    public List<User> getUserList(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return new ArrayList<>();
        serviceLocator.getISessionService().validate(session);
        return serviceLocator.getIUserService().getUserList();
    }
}
