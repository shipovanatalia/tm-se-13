package ru.shipova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.api.endpoint.ISessionEndpoint;
import ru.shipova.tm.dto.Result;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.exception.AccessForbiddenException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(name = "SessionEndpoint", targetNamespace = "http://endpoint.tm.shipova.ru/")
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint() {
        super(null);
    }

    public SessionEndpoint(@Nullable final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    @Nullable
    public Session openSession(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password) {
        if (serviceLocator == null) return null;
        return serviceLocator.getISessionService().open(login, password);
    }

    @Override
    @WebMethod
    @Nullable
    public Result closeSession(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return null;
        serviceLocator.getISessionService().validate(session);
        return serviceLocator.getISessionService().close(session);
    }
}
