package ru.shipova.tm.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.entity.Project;

import java.util.List;

public interface ProjectRepository {

    @Insert("INSERT INTO project (id, status, dateCreate, description, name, user_id) " +
            "VALUES (#{id}, #{status}, #{dateOfCreate}, #{description}, #{name}, #{userId})")
    void persist(@NotNull final Project project);

    @Update("UPDATE project SET dateBegin = #{dateOfBegin}, dateEnd = #{dateOfEnd}, status = #{status}," +
            "description = #{description}, name = #{name}, user_id = #{userId}" +
            " WHERE id = #{id}")
    void update(@NotNull final Project project);

    @Delete("DELETE FROM project WHERE user_id = #{arg0}")
    void removeAllByUserId(@NotNull final String userId);

    @Delete("DELETE FROM project WHERE id = #{arg0}")
    void removeByProjectId(@NotNull final String projectId);

    @Select("SELECT * FROM project WHERE user_id = #{arg0}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateOfCreate", column = "dateCreate"),
            @Result(property = "dateEnd", column = "dateOfEnd"),
            @Result(property = "status", column = "status")
    })
    List<Project> getProjectListByUserId(@NotNull final String userId);

    @Select("SELECT * FROM project WHERE id = #{arg0}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateOfCreate", column = "dateCreate"),
            @Result(property = "dateOfEnd", column = "dateEnd"),
            @Result(property = "status", column = "status")
    })
    Project findByProjectId(@NotNull final String projectId);

    @Select("SELECT * FROM project WHERE name = #{arg0}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateOfCreate", column = "dateCreate"),
            @Result(property = "dateOfEnd", column = "dateEnd"),
            @Result(property = "status", column = "status")
    })
    Project getProjectByName(@NotNull final String projectName);

    @Select("SELECT * FROM project WHERE user_id = #{userId} AND (name LIKE CONCAT('%', #{partOfData}, '%') OR description LIKE CONCAT('%', #{partOfData}, '%'))")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateOfCreate", column = "dateCreate"),
            @Result(property = "dateOfEnd", column = "dateEnd"),
            @Result(property = "status", column = "status")
    })
    List<Project> search(@Param(value = "userId") @NotNull String userId,
                         @Param(value = "partOfData") @NotNull String partOfData);


    @Select("SELECT * FROM project WHERE user_id = #{arg0} ORDER BY status")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateOfCreate", column = "dateCreate"),
            @Result(property = "dateOfEnd", column = "dateEnd"),
            @Result(property = "status", column = "status")
    })
    List<Project> selectProjectListByStatus(@Nullable final String userId);

    @Select("SELECT * FROM project WHERE user_id = #{arg0} ORDER BY dateCreate")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateOfCreate", column = "dateCreate"),
            @Result(property = "dateOfEnd", column = "dateEnd"),
            @Result(property = "status", column = "status")
    })
    List<Project> selectProjectListByDateOfCreate(@Nullable final String userId);

    @Select("SELECT * FROM project WHERE user_id = #{arg0} ORDER BY dateBegin")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateOfCreate", column = "dateCreate"),
            @Result(property = "dateOfEnd", column = "dateEnd"),
            @Result(property = "status", column = "status")
    })
    List<Project> selectProjectListByDateOfBegin(@Nullable final String userId);

    @Select("SELECT * FROM project WHERE user_id = #{arg0} ORDER BY dateEnd")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateOfCreate", column = "dateCreate"),
            @Result(property = "dateOfEnd", column = "dateEnd"),
            @Result(property = "status", column = "status")
    })
    List<Project> selectProjectListByDateOfEnd(@Nullable final String userId);
}
