package ru.shipova.tm.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.entity.Task;

import java.util.List;

public interface TaskRepository{

    @Insert("INSERT INTO task (id, status, dateCreate, description, name, project_id, user_id) " +
            "VALUES (#{id}, #{status}, #{dateOfCreate}, #{description}, #{name}, #{projectId}, #{userId})")
    void persist(@NotNull final Task task);

    @Update("UPDATE task SET dateBegin = #{dateOfBegin}, dateEnd = #{dateOfEnd}, status = #{status}," +
            "description = #{description}, name = #{name}, project_id = #{projectId}, user_id = #{userId}" +
            " WHERE id = #{id}")
    void update(@NotNull final Task task);

    @Delete("DELETE FROM task WHERE id = #{arg0}")
    void removeByTaskId(@NotNull final String taskId);

    @Delete("DELETE FROM task WHERE project_id = #{arg0}")
    void removeAllTasksOfProject(@NotNull final String projectId);

    @Delete("DELETE FROM task WHERE user_id = #{arg0}")
    void removeAllByUserId(@NotNull final String userId);

    @Select("SELECT * FROM task WHERE id = #{arg0}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateOfCreate", column = "dateCreate"),
            @Result(property = "dateOfEnd", column = "dateEnd"),
            @Result(property = "status", column = "status")
    })
    Task findByTaskId(@NotNull final String id);

    @Select("SELECT * FROM task WHERE user_id = #{arg0}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateOfCreate", column = "dateCreate"),
            @Result(property = "dateEnd", column = "dateOfEnd"),
            @Result(property = "status", column = "status")
    })
    List<Task> findAllByUserId(@NotNull final String userId);

    @Select("SELECT * FROM task WHERE name = #{arg0}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateOfCreate", column = "dateCreate"),
            @Result(property = "dateOfEnd", column = "dateEnd"),
            @Result(property = "status", column = "status")
    })
    Task getTaskByName(@NotNull final String taskName);

    @Select("SELECT * FROM task WHERE project_id = #{arg0}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateOfCreate", column = "dateCreate"),
            @Result(property = "dateOfEnd", column = "dateEnd"),
            @Result(property = "status", column = "status")
    })
    List<Task> showAllTasksOfProject(@NotNull final String projectId);

//    @Select("SELECT * FROM task WHERE user_id = #{arg0} AND (name LIKE '%#{arg1}%' OR description LIKE '%#{arg1}%')")
//    @Results(value = {
//            @Result(property = "id", column = "id"),
//            @Result(property = "userId", column = "user_id"),
//            @Result(property = "projectId", column = "project_id"),
//            @Result(property = "name", column = "name"),
//            @Result(property = "description", column = "description"),
//            @Result(property = "dateOfCreate", column = "dateCreate"),
//            @Result(property = "dateOfEnd", column = "dateEnd"),
//            @Result(property = "status", column = "status")
//    })
//    List<Task> search(@Nullable final String userId, @NotNull final String partOfData);

    @Select("SELECT * FROM task WHERE user_id = #{userId} AND (name LIKE CONCAT('%', #{partOfData}, '%') OR description LIKE CONCAT('%', #{partOfData}, '%'))")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateOfCreate", column = "dateCreate"),
            @Result(property = "dateOfEnd", column = "dateEnd"),
            @Result(property = "status", column = "status")
    })
    List<Task> search(@Param(value = "userId") @NotNull String userId,
                      @Param(value = "partOfData") @NotNull String partOfData);

    @Select("SELECT * FROM task WHERE user_id = #{arg0} ORDER BY status")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateOfCreate", column = "dateCreate"),
            @Result(property = "dateOfEnd", column = "dateEnd"),
            @Result(property = "status", column = "status")
    })
    List<Task> selectTaskListByStatus(@Nullable final String userId);

    @Select("SELECT * FROM task WHERE user_id = #{arg0} ORDER BY dateCreate")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateOfCreate", column = "dateCreate"),
            @Result(property = "dateOfEnd", column = "dateEnd"),
            @Result(property = "status", column = "status")
    })
    List<Task> selectTaskListByDateOfCreate(@Nullable final String userId);

    @Select("SELECT * FROM task WHERE user_id = #{arg0} ORDER BY dateBegin")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateOfCreate", column = "dateCreate"),
            @Result(property = "dateOfEnd", column = "dateEnd"),
            @Result(property = "status", column = "status")
    })
    List<Task> selectTaskListByDateOfBegin(@Nullable final String userId);

    @Select("SELECT * FROM task WHERE user_id = #{arg0} ORDER BY dateEnd")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateOfCreate", column = "dateCreate"),
            @Result(property = "dateOfEnd", column = "dateEnd"),
            @Result(property = "status", column = "status")
    })
    List<Task> selectTaskListByDateOfEnd(@Nullable final String userId);
}
