package ru.shipova.tm.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.shipova.tm.entity.User;

import java.util.List;

public interface UserRepository {

    @Insert("INSERT INTO user (id, login, passwordHash, role) VALUES (#{id}, #{login}, #{passwordHash}, #{roleType})")
    void persist(@NotNull final User user);

    @Select("SELECT * FROM user WHERE login = #{login}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "passwordHash", column = "passwordHash"),
            @Result(property = "roleType", column = "role")
    })
    User findByLogin(@NotNull final String login);

    @Select("SELECT * FROM user WHERE id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "passwordHash", column = "passwordHash"),
            @Result(property = "roleType", column = "role")
    })
    User findById(@NotNull final String id);

    @Update("UPDATE user SET login = #{login}, passwordHash = #{passwordHash}, role = #{roleType} WHERE id = #{id}")
    void update(@NotNull final User user);

    @Select("SELECT * FROM user")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "passwordHash", column = "passwordHash"),
            @Result(property = "roleType", column = "role")
    })
    List<User> findAll();
}
