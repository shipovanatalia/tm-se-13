package ru.shipova.tm.repository;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.jetbrains.annotations.NotNull;
import ru.shipova.tm.entity.Session;

public interface SessionRepository {

    @Insert("INSERT INTO session (id, signature, timestamp, user_id) VALUES (#{id}, #{signature}, #{timestamp}, #{userId})")
    void persist(@NotNull final Session session);

    @Delete("DELETE FROM session WHERE id = #{arg0}")
    void removeBySessionId(@NotNull final String sessionId);

    @Select("SELECT * FROM session WHERE id = #{arg0}")
    @Results(value = {
            @org.apache.ibatis.annotations.Result(property = "id", column = "id"),
            @org.apache.ibatis.annotations.Result(property = "signature", column = "signature"),
            @org.apache.ibatis.annotations.Result(property = "timestamp", column = "timestamp"),
            @org.apache.ibatis.annotations.Result(property = "userId", column = "user_id")
    })
    Session findBySessionId(@NotNull final String sessionId);
}
